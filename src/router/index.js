import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage').default
    },
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/manualBackUp',
      name: 'manualBackUp',
      component: require('@/components/ManualBackUp').default
    },
    {
      path: '/groups',
      name: 'Groups',
      component: require('@/components/Groups').default
    },
    {
      path: '/editGroup',
      name: 'editGroup',
      component: require('@/components/Groups').default
    },
  ]
})
