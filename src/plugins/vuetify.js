import Vue from "vue";
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

export default new Vuetify({});
